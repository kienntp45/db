﻿using ksqlDB.RestApi.Client.KSql.RestApi.Statements;
using ksqlDB.RestApi.Client.KSql.RestApi;

namespace SpeedkKsql.Service
{
    public class KsqlCrud
    {
        public static int times = 0;

        public static async Task<bool> CreateTablesAsync(KSqlDbRestApiClient _kSqlDbRestApiClient)
        {
            try
            {
                var createMoviesTable = $@"CREATE TABLE longpv2s (id int PRIMARY KEY,v1 int,v2 int,v3 int,v4 int, v5 int,v6 int, v7 int, v8 int , v9 int) WITH (KAFKA_TOPIC='longpv',PARTITIONS=1,VALUE_FORMAT = 'JSON');";
                var ksqlDbStatement = new KSqlDbStatement(createMoviesTable);
                await _kSqlDbRestApiClient.ExecuteStatementAsync(ksqlDbStatement);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static async Task Insert(KSqlDbRestApiClient kSqlDbRestApiClient,string ksqlDbUrl)
        {
            int a = 1;
            for (int i = times * 100 + 1; i < (times + 1) * 100 + 1; i++)
            {
                if (a == 200 || a == (times + 1) * 100)
                {
                    var httpClientFactory = new HttpClientFactory(new Uri(ksqlDbUrl));
                    kSqlDbRestApiClient = new KSqlDbRestApiClient(httpClientFactory);
                    a = 1;
                }
                var insert = $@"INSERT INTO longpv2s VALUES ({i},{i},{i},{i},{i},{i},{i},{i},{i},{i});";
                var ksqlDbStatement = new KSqlDbStatement(insert);
                await kSqlDbRestApiClient.ExecuteStatementAsync(ksqlDbStatement);
                a++;
            }
            times++;
            Console.WriteLine("end");
        }
    }
}
