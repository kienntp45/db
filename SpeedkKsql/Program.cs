﻿using ksqlDB.RestApi.Client.KSql.Config;
using ksqlDB.RestApi.Client.KSql.Linq;
using ksqlDB.RestApi.Client.KSql.Query.Context;
using ksqlDB.RestApi.Client.KSql.Query.Context.Options;
using ksqlDB.RestApi.Client.KSql.Query.Options;
using ksqlDB.RestApi.Client.KSql.RestApi;
using ksqlDB.RestApi.Client.KSql.RestApi.Parameters;
using SpeedkKsql;
using SpeedkKsql.Models;
using SpeedkKsql.Service;
using System.Diagnostics;
using System.Reactive.Linq;

namespace ksqlDb.Program;
public class Program
{
    public static KSqlDBContextOptions CreateQueryStreamOptions(string ksqlDbUrl)
    {
        var contextOptions = new KSqlDbContextOptionsBuilder()
          .UseKSqlDb(ksqlDbUrl)
          .SetBasicAuthCredentials("fred", "letmein")
          .SetJsonSerializerOptions(jsonOptions =>
          {
              jsonOptions.IgnoreReadOnlyFields = true;

          })
          .SetProcessingGuarantee(ProcessingGuarantee.ExactlyOnce)
          .SetupQueryStream(options =>
          {
              options.AutoOffsetReset = AutoOffsetReset.Earliest;
          })
          .SetupQuery(options =>
          {
              options.Properties[KSqlDbConfigs.ProcessingGuarantee] = ProcessingGuarantee.ExactlyOnce.ToKSqlValue();
          })
          .Options;
        contextOptions.DisposeHttpClient = false;
        return contextOptions;
    }
    public static string ksqlDbUrl = @"http://10.26.4.81:8088/";
    public static List<longpv2s> lst = new List<longpv2s>();

    public Program()
    {

    }

    public static async Task Main()
    {
        var contextOptions = new KSqlDBContextOptions(ksqlDbUrl);
        await using var context = new KSqlDBContext(contextOptions);
        var httpClientFactory = new HttpClientFactory(new Uri(ksqlDbUrl));   
        var _kSqlDbRestApiClient1 = new KSqlDbRestApiClient(httpClientFactory);  

        for (int i = 0; i < 10000; i++)
        {
           
            var input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    await KsqlCrud.CreateTablesAsync(_kSqlDbRestApiClient1);
                    break;

                case "2":
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    await KsqlCrud.Insert(_kSqlDbRestApiClient1,ksqlDbUrl);
                    sw.Stop();
                    Console.WriteLine("het " + sw.ElapsedMilliseconds);
                    break;

                case "3":
                    string kSql = "select * from longpv2s emit changes;";
                    QueryParameters queryParameters = new QueryParameters
                    {
                        Sql = kSql,
                        [QueryParameters.AutoOffsetResetPropertyName] = "earliest",
                    };
                    Stopwatch sw1 = new Stopwatch();
                    sw1.Start();
                    var user = context.CreateQuery<longpv2s>(queryParameters)
                          .ToObservable();
                    user
                          .Subscribe(onNext: evt =>
                          {
                              Console.WriteLine(nameof(longpv2s) + ": " + evt.Id);
                              Console.WriteLine();
                              lst.Add(evt);
                          }
                          , onError: error => { Console.WriteLine($"Exception: {error.Message} "); }
                          , onCompleted: () => Console.WriteLine($"Completed er")); 
                    break;

                default:
                    break;
            }
        }
    }
}

