﻿using mongo1.Models;
using mongo1.Service;
using System.Diagnostics;

namespace mongo;
public class Program
{
    private readonly static string connectionString = "mongodb://admin:admin@localhost:27017/";
    private readonly static string databaseName = "Person";
    private readonly static string colletionName = "Person";
    static void Main()
    {
        var db = new MongoCRUD(connectionString,databaseName);
        for (int j = 0; j < 10000; j++)
        {
            Console.WriteLine("1: Insert table");
            Console.WriteLine("2: Get table");
            Console.Write("nhap ");
            var input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    var lst = new List<Person>();
                    for (int i = 1; i <= 100; i++)
                    {
                        var person = new Person(i, i, i, i, i, i, i, i, i);
                        lst.Add(person);
                    }
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    db.InsertRecord(lst,colletionName);
                    sw.Stop();
                    Console.WriteLine(sw.ElapsedMilliseconds);
                    break;
                case "2":
                    Stopwatch sw1 = new Stopwatch();
                    sw1.Start();
                    db.LoadRecord(colletionName);
                    sw1.Stop();
                    Console.WriteLine(sw1.ElapsedMilliseconds);
                    break;
                default:
                    break;
            }
        }
    }
}