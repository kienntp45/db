﻿using mongo1.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace mongo1.Service
{
    public class MongoCRUD
    {
        readonly IMongoDatabase _db;
        public MongoCRUD(string connectionString,string databaseName)
        {
            var client = new MongoClient( connectionString);
            _db = client.GetDatabase(databaseName);
        }

        public void InsertRecord( List<Person> person,string colletionName)
        {
            var collection = _db.GetCollection<Person>( colletionName);
            collection.InsertMany(person);
        }

        public List<Person> LoadRecord(string colletionName)
        {
            var collection = _db.GetCollection<Person>(colletionName);
            return collection.Find(new BsonDocument()).ToList();
        }
    }
}
