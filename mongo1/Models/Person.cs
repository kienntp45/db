﻿using MongoDB.Bson.Serialization.Attributes;

namespace mongo1.Models
{
    public class Person
    {
        [BsonId]
        public Guid Id { get; set; }
        public int v1 { get; set; }
        public int v2 { get; set; }
        public int v3 { get; set; }
        public int v4 { get; set; }
        public int v5 { get; set; }
        public int v6 { get; set; }
        public int v7 { get; set; }
        public int v8 { get; set; }
        public int v9 { get; set; }

        public Person(int v1, int v2, int v3, int v4, int v5, int v6, int v7, int v8, int v9)
        {

            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
            this.v4 = v4;
            this.v5 = v5;
            this.v6 = v6;
            this.v7 = v7;
            this.v8 = v8;
            this.v9 = v9;
        }
        public Person()
        {

        }
    }
}
